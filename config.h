/* ~/.dmenu/config.h */

/* Specifies Defaults: Can Be Overwritten When Spawning DMENU */

static int topbar = 1; /* Spawn DMENU: 1 = Top / 0 = Bottom */

/* -fn  */
static const char *fonts[] = {
	"SauceCodePro Nerd Font Mono:size=10:antialias=true"
};

/* -p */
static const char *prompt = "Search:"; 

/* Color-Variables: Background = -nb / Foreground = -nf / Selected Background = -sb / Selected Foreground = -sf */
const char bg[8] = "#02122b"; /* Background */
const char sl[8] = "#052c6b"; /* Selection */
const char ad[8] = "#04204d"; /* Adjacent */
const char tx[8] = "#729fcf"; /* Text */
const char hl[8] = "#8cc4ff"; /* Highlight */

static const char *colors[SchemeLast][2] = {
				 /* fg , bg */
	[SchemeNorm] 		= { tx , bg },
	[SchemeSel] 		= { tx , sl },
	[SchemeMid]             = { tx , ad },
	[SchemeNormHighlight]   = { hl , bg },
	[SchemeSelHighlight] 	= { hl , sl },
	[SchemeMidHighlight] 	= { hl , ad },
	[SchemeOut]             = { "#000000", "#00ffff" }
};

/* -l */ 
static unsigned int lines      = 0; /* Horizontal Bar = 0 / Vertical List With n > 0 Lines = n */

static unsigned int lineheight = 22;

/* -h */
static unsigned int min_lineheight = 22;

static const char worddelimiters[] = " "; /* Characters Not Considered Part Of Word While Deleting Words - Example: " /?\"&[]" */
